'use strict';

// function output(txt) {
// 	console.log(txt);
// }

const output = console.log;

// predicate is a unary function (commonly) which returns a boolean value
// function printIf(shouldPrintIt) {
// 	return function(msg) {
// 		if (shouldPrintIt(msg)) {
// 			output(msg);
// 		}
// 	};
// }

// when utility: function that calls another function if a predicate evaluates to true,
// like a functional if-else

const when = fn => predicate => (...args) => (predicate(...args) ? fn(...args) : null);

// printIf(isShortEnough)(msg1); // Hello
// when(output)(isShortEnough)(msg1);
const printIf = when(output);

const not = fn => (...args) => !fn(...args);

const isShortEnough = str => str.length <= 5;

// function isLongEnough(str) {
// 	return !isShortEnough(str);
// }

// expressed in point-free
const isLongEnough = not(isShortEnough);

var msg1 = 'Hello';
var msg2 = msg1 + ' World';

printIf(isShortEnough)(msg1); // Hello
printIf(isShortEnough)(msg2);
printIf(isLongEnough)(msg1);
printIf(isLongEnough)(msg2); // Hello World
