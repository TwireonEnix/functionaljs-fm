'use strict';

function increment(x) {
	return x + 1;
}
function decrement(x) {
	return x - 1;
}
function double(x) {
	return x * 2;
}
function half(x) {
	return x / 2;
}

const reverse = fn => (...args) => fn(...args.reverse());

// modify these:
// composing by using a reduceRight native js function. The reduceRight
// behaves as a normal reduce function, but it reverses the order of the
// reduce inputs.
// const compose = (...fns) => v => fns.reduceRight((x, f) => f(x), v);
// const pipe = reverse(compose);

// using pipe to compose first:
const pipe = (...fns) => v => fns.reduce((x, f) => f(x), v);
const compose = reverse(pipe);

var f1 = compose(
	increment,
	decrement,
);
var f2 = pipe(
	decrement,
	increment,
);
var f3 = compose(
	decrement,
	double,
	increment,
	half,
);
var f4 = pipe(
	half,
	increment,
	double,
	decrement,
);
var f5 = compose(increment);
var f6 = pipe(increment);

console.log(f1(3) === 3);
console.log(f1(3) === f2(3));
console.log(f3(3) === 4);
console.log(f3(3) === f4(3));
console.log(f5(3) === 4);
console.log(f5(3) === f6(3));

// # Composition

// This is an exercise to practice composition.
// ## Instructions
// 1. Define a `compose(..)` that takes any number of functions (as individual arguments) and composes them right-to-left.
// 2. Define a `pipe(..)` that takes any number of functions (as individual arguments) and composes them left-to-right.
