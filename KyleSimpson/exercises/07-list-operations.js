'use strict';

// 1)
const five = () => 5;
const two = () => 2;

// 2)
const add = (a, b) => a + b;

const res1 = add(five(), two());
console.log({ res1 });

// 3)
const add2 = (fn1, fn2) => add(fn1(), fn2());
const res2 = add2(five, two);
console.log({ res2 });

// 4)
// This is called a constant value in fp libraries, when we call with an arg, the parameter stays
// the same
const constant = v => () => v;
const fiveB = constant(5);
const twoB = constant(2);

const res3 = add2(fiveB, twoB);
console.log({ res3 });

// 5)
// iterative solution
const addN1 = (...fns) => {
	let cpy = [...fns];
	while (cpy.length > 2) {
		let [fn0, fn1, ...rest] = cpy;
		cpy = [() => add2(fn0, fn1), ...rest];
	}
	return add2(cpy[0], cpy[1]);
};
const res4 = addN1(constant(3), constant(7), five, two, constant(18));
console.log({ res4 });

// recursive approach
const addN2 = (...fns) => {
	const [fn0, fn1, ...rest] = fns;
	if (rest.length > 0) return addN2(() => add2(fn0, fn1), ...rest);
	return add2(fn0, fn1);
};
const res5 = addN2(constant(3), constant(7), five, two, constant(18));
console.log({ res5 });

// if the vanilla reduce function does not get the second parameter, it takes the first
// one as the first input and start reducing from the second.
const addN3 = fns => fns.reduce((composedSum, fn) => () => add2(composedSum, fn));
const res6 = addN3([constant(3), constant(7), five, two, constant(18)])();
console.log({ res6 });

// 6)
const numbers = [
	2,
	3,
	4,
	5,
	2,
	4,
	23,
	4,
	4,
	5,
	4,
	54,
	65,
	3,
	32,
	12,
	10,
	1,
	0,
	2,
	3,
	4,
	5,
	2,
	3,
	2,
	32,
	3,
	5,
	6,
	4,
	5,
	55,
	22,
];

// we need to use reduce to filter the uniques.
const uniques = numbers.reduce((newList, number) => (newList.includes(number) ? newList : [...newList, number]), []);

// 7)
// const sortedAndEvens = uniques.sort((a, b) => a - b).filter(v => v % 2 === 0);
// console.log(sortedAndEvens);
// just learned that sort is not fp safe because it mutates the original array!
const evens = uniques.filter(v => v % 2 === 0);

// 8)
// original fn: n => constant(n), but with point free:
const mappedFnsToNumbers = evens.map(constant);
const res7 = addN3(mappedFnsToNumbers)();
console.log({ res7 });

// const addedNumbersFinal

// # Lists (And FP Review!)
// This is an exercise to practice list operations (map/reduce/filter). We also revisit
//  a variety of previous FP concepts (closure, recursion, etc).
// ## Instructions

// 1. Write two functions, each which return a fixed number (different from each other)
//  when called.
// 2. Write an `add(..)` function that takes two numbers and adds them and returns the
// result. Call `add(..)` with the results of your two functions from (1) and print the result to the console.
// 3. Write an `add2(..)` that takes two functions instead of two numbers, and it calls
//  those two functions and then sends those values to `add(..)`, just like you did in (2) above.
// 4. Replace your two functions from (1) with a single function that takes a value and
//  returns a function back, where the returned function will return the value when it's called.
// 5. Write an `addn(..)` that can take an array of 2 or more functions, and using only
//  `add2(..)`, adds them together. Try it with a loop. Try it without a loop (recursion).
//  Try it with built-in array functional helpers (hint: reduce).
// 6. Start with an array of odd and even numbers (with some duplicates), and
// trim it down to only have unique values.
// 7. Filter your array to only have even numbers in it.
// 8. Map your values to functions, using (4), and pass the new list of functions
// to the `addn(..)` from (5).
// ## Bonus
// Write tests for your functions.
