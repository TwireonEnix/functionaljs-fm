 There are a difference between authentically functions and procedures.

 All functions must get some input and return some output. 
 The function keyword is not all about functional programming. If a function that is apparently
 pure calls an impure function (procedure) the function is polluted by it and automatically
 becomes a procedure as well.

 A fundamental characteristic about FP is that we cannot do FP with something that is not 
 strictly a function. 'a 1 must only be a 1 in a 1+1 to be able to get 2'.

 Function is a semantic relationship between the input and the computed output.

 function f(x) {
   return 2 * (x**2) + 3;
 }

 The goal is to create relationships that are easy to read. Every thing that we use, are a
 building block of the program. We should take this kind of thought to all code we read and 
 write: is it a function or not?

Even undefined is a valid output, IF there's a semantic relationship between the input and the
output. If there's not we are not doing functional programming.

SIDE EFFECTS?

Also the inputs and outputs to take benefit of FP have to be direct. Side effects refer to 
indirect inputs, or indirect outputs, or both. When a procedure affects something outside of
itself. It can't access anything outside of itself, or set or modify anything outside of itself.

In javascript we cannot say a function is pure or not from the function definition itself.
The important thing to note is 'the call'. The conditions in which the functions are called
can tell whether the it is the functional way or not.

Examples: 
- I/O (console, files, etc);
- Database Storage
- Network calls
- DOM
- Timestamps
- Random numbers.

'Any observable change in the system environment'

Is theoretically impossible to avoid all side effects, and FP is all about avoiding side effects,
but we have very intentional about them and not only not care about them at all. Side effects will
occur in every program, FP is about tightly controlling side effects. There's no such thing as 
'no side effects'. We must avoid them where possible, and make them obvious otherwise. Bugs are 
more likely to happen in the 'side effects' section of the code.

PURE FUNCTIONS

A pure function is the function in that uses all its input, compute an output that has a semantic
correlation with the inputs and has no side effects.
Remember: It's not about the function definition itself only, is also about the function call.
That's what we are looking for, pure function calls.

Example:

// pure
function addTwo(x, y) {
  return x + y;
} 

// impure
function addAnother(x, y) {
  return addTwo(x,y) + z;
}

Using an outside value or a 'dependency' we cannot predict the behavior of the function.
However, is it really impure? Take this into consideration:

const z = 1;

function addAnother(x, y) {
  return addTwo(x, y) + z;
}

We need to check if the z is reassigned again, we have to look at the whole code.
'Functions are first class citizens in javascript'.

However, we can reduce the surface area with currying and partial application.

function addAnother(z) {
  return function addTwo(x, y) {
    return x + y + z;
  }
} 

PURE FUNCTION CALLS ACT IN ISOLATION

Function purity is a level of confidence: how confident are we on its behavior

SIDE EFFECTS
Water down the benefits of functional programming, but they're unavoidable. There is no such
thing as 'no side effects'. Avoid them where possible, make them obvious otherwise.

EXTRACTING IMPURITY
Is there a way for me to refactor this function and isolate its side effects?