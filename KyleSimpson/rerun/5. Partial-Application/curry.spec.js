const R = require('ramda');
const { curry } = require('../../../functional-utils');
const { assert } = require('chai')
const sum3 = (a, b, c) => a + b + c;
const add = (x, y) => x + y;

describe('05. Currying', () => {
  it('Example with automatic currying', () => {
    const sum = curry(sum3);
    const add23 = sum(2, 3);
    assert.equal(add23(5), 10);
  })

  it('Example with manual currying, strict currying Haskell like', () => {
    const sumHere = a => b => c => a + b + c;
    const add23 = sumHere(2)(3);
    assert.equal(add23(5), 10);
  })

  it('Partial application vanilla js', () => {
    /** This is an example of a partial application of the add function,
     * however it is more often to curry the original function and partially
     * apply some parameters first
     */
    const arrEx = [0, 2, 4, 6, 8];
    const partial = arrEx.map(v => add(1, v));
    const addH = R.curry(add);
    const curried = arrEx.map(addH(1));
    assert.deepEqual(partial, curried);
  });

  it('Partial application with Ramda', () => {
    const partialAdd5 = R.partial(sum3, [2, 3]);
    assert.equal(partialAdd5(10), 15);
  })

})