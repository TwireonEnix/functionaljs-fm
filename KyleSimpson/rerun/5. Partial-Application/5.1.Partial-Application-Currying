Theres a way of partially apply some values to a function, there's this utility called
'partial' that we can use to fix value parameters to a function.
Example:

function ajax (url, data, cb) { ... }

const getCustomer = partial(ajax, url);

getCurrentCustomer = partial(getCustomer, data);

getCurrentCustomer(renderCustomer);

However, the other way of specializing a function is way more common in the fp world and
it's called currying.
Currying is a technique born in haskell, where all functions are curried by being unary,
in which we pass one argument at a time, each time an argument is passed to the function
it will return another more specialized function with the previous parameter fixed and 
remembered via closure which expects the next input, and that will follow the same pattern
until all of its parameters are given. Only then, it will return the result of the completed function.

Using the example above:

Original: 
function ajax (url, data, cb) { ... }

Curried version:
function ajax (url) {
  return function getData (data) {
    return function getCB (cb) { ... }
  }
}

This is a manual currying. Currying can be achieved with an utility function that frankly looks
like a magic spell. Some libraries have two versions of currying, an automatic currying, or a fixed N
currying.

Ok, then we can use the curried function to achieve specialization:

const getCustomer = ajax(CUSTOMER_API);
const getCurrentUser = getCustomer({ id: 42 });
const getCurrentCustomer = getCurrentUser(renderCustomer);

All ramda utility functions are curried by default and the data they operate over is always the
final input of the function.

Partial Application vs Currying:

1. Both are specialization techniques.
2. Partial Application presets some arguments now and receives the rest on the next call.
3. Currying does not preset any arguments, receives each argument one at a time.
