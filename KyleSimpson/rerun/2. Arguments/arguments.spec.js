const { assert } = require('chai');

const add = (a, b) => a + b;
const f = (...args) => args;

describe('02. Arguments, Adapters', () => {
  it('Arity adaptation', () => {
    // Used to modify the shape of a function. Unary and binary fn above are adapter functions.

    // Every fn in javascript is variadic in the sense that we can pass any number of args to its
    // parameters and javascript does not complain.
    function unary(fn) {
      return function one(arg) {
        return fn(arg);
      };
    }
    // compact way with arrow fns. Kyle dislike this.
    const binary = fn => (arg1, arg2) => fn(arg1, arg2);

    const one = unary(f);
    const two = binary(f);
    assert.deepEqual(one(1, 2, 3), [1]);
    assert.deepEqual(two(1, 2, 3), [1, 2]);
  });

  it('Flip', () => {
    //always flip only the first two
    const flip = fn => (arg1, arg2, ...args) => fn(arg2, arg1, ...args);

    const flipFn = flip(f);
    assert.deepEqual(flipFn(1, 2, 3), [2, 1, 3]);
  });

  it('Reversed Args', () => {
    const reverse = fn => (...args) => fn(args.reverse());

    const rev = reverse(f);
    assert(rev(1, 2, 3), [3, 2, 1]);
  });

  it('Apply', () => {
    const addFour = (x, y, z, w) => x + y + z + w;
    const apply = fn => args => fn(...args);
    const sum = apply(addFour);
    const num = [1, 2, 3, 4];
    assert.equal(sum(num), 10);

    // Fn.prototype.apply is weird for me, the first parameter expect a context object, second is the array of args
    // that will be assigned in that order to the parameters of the function.
    assert.equal(sum(num), addFour.apply('', num));
  });

  it('Unapply', () => {
    // my implementation of un-apply, the inverse operation of apply in which the arguments are passed to a
    // variadic function that will pass to a function that expects an array as input.
    const unapply = fn => (...args) => fn(args);
    const sumArr = arr => arr.reduce(add, 0);
    const sumN = unapply(sumArr);

    assert.equal(sumN(1, 2, 3), 6);
  });
});
