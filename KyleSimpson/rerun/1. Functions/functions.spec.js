const R = require('ramda');
const { assert } = require('chai');

const toAssert = R.curry((v, eq) => assert.equal(v, eq));

describe('01. Functions', () => {
  it('IMPURE Add numbers', () => {
    /** Is this a function?
     * It's kind of like a function right? Not exactly. We could see the function world as
     * a Venn diagram, The universe is something that we need to name, and a function is a set
     * which is part of that universe, but this particular block of code does not fully qualify
     * to be named 'a function'. Best term we can use is that is a procedure. Any functions that
     * does not have a return keyword is not a function.
     */
    function addNumbers(x = 0, y = 0, z = 0, w = 0) {
      const total = x + y + z + w;
      // console.log(total);
    }

    /** What about this one? Is it a function?
     * Functions can only call other functions, if they call procedures, they become procedures.
     * The procedure pollutes the function.
     */
    function extraNumbers(x = 2, ...args) {
      return addNumbers(x, 40, ...args);
    }

    toAssert(extraNumbers())(); // Doesn't return any value, so it's undefined, but logs 42
    extraNumbers(3, 8, 11); // logs 62, but is not a function.
  });

  it('IMPURE Shipping Rate', () => {
    /** This for me is an abomination, however i've seen programmers doing this shit. A function
     * must not have any effect outside of its scope. These are side effects and those invalidate
     * all the benefits of FP.
     */
    function shippingRate() {
      rate = (size + 1) * weight + speed;
    }

    var rate,
      size = 12,
      weight = 4,
      speed = 5;
    shippingRate();
    toAssert(rate, 57);
    size = 8;
    speed = 6;
    shippingRate();
    toAssert(rate, 42);
  });

  it('PURE-IMPURE add', () => {
    // Pure
    const addTwo = (x, y) => x + y;

    //Impure
    const addAnotherA = (x, y) => addTwo(x, y) + z;

    /** But what if z is declared before? Then this is a pure function call, in these situations we
     * should have reviewed the whole context in the program to determine if we have a pure function call.
     * The var or const keyword adds nothing to the FP, the important issue to consider is: is the variable
     * being reassigned? not if it can or not be reassigned.
     * Const has a problem because it looks like we have a true constant, values that don't change when
     * actually it's making variables that cannot get reassigned.
     */
    var z = 1;
    const addAnotherB = (x, y) => addTwo(x, y) + z;

    toAssert(addAnotherB(20, 21), 42);
  });

  it('CONTAIN IMPURITY, actual exercise from the workshop', () => {
    const students = [
      { id: 260, name: 'Kyle' },
      { id: 729, name: 'Susan' },
      { id: 42, name: 'Frank' },
      { id: 74, name: 'Jessica' },
      { id: 491, name: 'Ally' }
    ];

    // do not change
    const sortStudentsByName = () => {
      students.sort((s1, s2) => (s1.name < s2.name ? -1 : s1.name > s2.name ? 1 : 0));
      return students;
    };

    // do not change
    const sortStudentsByID = () => {
      students.sort((s1, s2) => s1.id - s2.id);
      return students;
    };

    /** Solution
     * Using spread op, however it is proven that .slice() is more performant than this.
     * But the difference nowadays is neglectable
     */
    const getStudentsByName = studentsHere => [...sortStudentsByName([...studentsHere])];
    const getStudentsByID = studentsHere => [...sortStudentsByID([...studentsHere])];

    /** */

    var studentsTest1 = getStudentsByName(students);
    toAssert(studentsTest1[0].name, 'Ally');
    toAssert(studentsTest1[1].name, 'Frank');
    toAssert(studentsTest1[2].name, 'Jessica');
    toAssert(studentsTest1[3].name, 'Kyle');
    toAssert(studentsTest1[4].name, 'Susan');

    var studentsTest2 = getStudentsByID(students);
    toAssert(studentsTest2[0].id, 42);
    toAssert(studentsTest2[1].id, 74);
    toAssert(studentsTest2[2].id, 260);
    toAssert(studentsTest2[3].id, 491);
    toAssert(studentsTest2[4].id, 729);

    var studentsTest3 = students;
    toAssert(studentsTest3[0].id === 260, studentsTest3[0].name === 'Kyle');
    toAssert(studentsTest3[1].id === 729, studentsTest3[1].name === 'Susan');
    toAssert(studentsTest3[2].id === 42, studentsTest3[2].name === 'Frank');
    toAssert(studentsTest3[3].id === 74, studentsTest3[3].name === 'Jessica');
    toAssert(studentsTest3[4].id === 491, studentsTest3[4].name === 'Ally');
  });
});
