const { assert } = require('chai');
const { curry, compose } = require('../../../functional-utils');
const R = require('ramda');
const flip = fn => (arg1, arg2, ...args) => fn(arg2, arg1, ...args);

const isOdd = v => v % 2 === 1;
const isEven = v => !isOdd(v);

const not = fn => v => !fn(v); // naive R.not adapter, i think.
const mod = y => x => x % y;
const eq = y => x => x === y;

describe('03. Point-Free', () => {
  it('Writing a function based in other', () => {
    // here we used a function written in the terms of another. isEven in the terms of is odd;
    // this achieves to make a semantic relationship between these functions, and make that relationship
    // obvious
    assert.isTrue(isOdd(3));
    assert.isTrue(isEven(2));
  });

  it('Writing a point free version of isOdd with the help of an adapter', () => {
    // What we achieve with this? A more declarative style using point-free instead of the original isEven fn.
    /**
     * As a general rule, declarative programming is more implicit, imperative is explicit.
     * However, we can imply easily what map(double, [...]) do, than reading a for loop line by line and decipher
     * what it does.
     *
     * Using point free style here makes the relationship between isEven and isOdd much more clear.
     */
    const isEvenHere = not(isOdd);
    assert.isTrue(isEvenHere(2));
    assert.isFalse(isEvenHere(1));
  });

  it('Practice point free (course exercise).', () => {
    // utils // in my world pred is before the fn
    const when = fn => pred => (...args) => {
      if (pred(...args)) return fn(...args);
      return args;
    };

    const isShortEnough = str => str.length <= 5;
    const identity = v => v;

    // refactor printIf and isLongEnough to use point-free
    const printIf = curry((shouldPrintIt, msg) => {
      if (shouldPrintIt) return msg;
    });
    // const isLongEnough = str => !isShortEnough(str);

    /** -------------------- */
    const isLongEnough = not(isShortEnough);
    const printIfPF = when(identity); // course solution, but is kinda wrong, it has no sense semantically, pred should be first.

    /** rewrite with ramda */
    const printIfR = pred => R.when(pred, R.toUpper);

    const msg1 = 'Hello';
    const msg2 = `${msg1} World`;
    assert.equal(printIf(isShortEnough)(msg1), 'Hello');
    assert.equal(printIf(isLongEnough)(msg2), 'Hello World');
    assert.equal(printIfPF(isShortEnough)(msg1), 'Hello');
    assert.equal(printIfPF(isLongEnough)(msg2), 'Hello World');
    assert.equal(printIfR(isShortEnough)('Hello'), 'HELLO');
    assert.equal(printIfR(isLongEnough)('Hello'), 'Hello');
  });

  it('Define isOdd in a point free style with more basic utils like mod and equal', () => {
    const mod2 = mod(2); // partial application of the more general mod function.
    const eq1 = eq(1);

    /** This fn is more readable to me than the initial isOdd implementation. */
    const isOddHere = compose(eq1, mod2); // point-free using function composition
    const isEvenHere = not(isOddHere);
    assert.isTrue(isOddHere(1));
    assert.isFalse(isEvenHere(1));
  });
});