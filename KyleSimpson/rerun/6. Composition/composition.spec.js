const { assert } = require('chai')
const { compose, reverse } = require('../../../functional-utils'); // poor man's compose written with reduceRight
const pipe = reverse(compose);

const add = x => y => x + y;
const minus2 = x => x - 2;
const triple = x => x * 3;
const inc = x => x + 1;

const basePrice = 10;
const arbitraryRatio = 4
describe('06. Composition', () => {
  it('Example of imperative style', () => {
    // calculate shipping rate;
    let tmp = inc(arbitraryRatio);
    tmp = triple(tmp);
    const totalCost = basePrice + minus2(tmp);
    assert.equal(totalCost, 23)
  });

  it('Example using common function composition without variables', () => {
    // hard to reason about
    const totalCost = basePrice + minus2(triple(inc(4)));
    assert.equal(totalCost, 23);
  });

  it('Example abstracting the poor mans compose into a function', () => {
    const shippingRate = x => minus2(triple(inc(x))); // this is an abstraction of a composition
    const totalCost = basePrice + shippingRate(4);
    assert.equal(totalCost, 23);
  });

  it('Example of functional composition', () => {
    // composition executes rtl, while pipe goes the other way.
    // compose is a high order function that produces another function.
    const calculateTotalCost = compose(add(basePrice), minus2, triple, inc);
    // orderMatters
    const fns = [inc, triple, minus2];
    // somewhat convoluted, but was a trivial example, not a real world ex.
    const otherCost = compose(...[add(basePrice), ...fns]);
    const withPipe = pipe(...[...fns, add(basePrice)]);

    assert.equal(calculateTotalCost(arbitraryRatio), 23)
    assert.equal(otherCost(arbitraryRatio), 17);
    assert.equal(calculateTotalCost(arbitraryRatio), withPipe(arbitraryRatio));
  });

})