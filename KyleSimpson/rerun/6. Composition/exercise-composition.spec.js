const { assert } = require('chai')
const { reduce, reverse } = require('../../../functional-utils');
/** COMPOSITION
 * 1. Define a 'compose' that takes any number of function as individual arguments and composes
 * them rtl
 * 2. Define a 'pipe' function.
 */

/** My Implementation */
const myPipe = (...fns) => v => reduce((x, f) => f(x), v, fns);
const myCompose = reverse(myPipe);

/** Kyle Implementation */
function kPipe(...fns) {
  return function piped(v) {
    for (let fn of fns) {
      v = fn(v);
    }
    return v
  }
}

function kCompose(...fns) {
  return kPipe(...fns.reverse());
}

/** I'll try to write a compose not using the native prototype reduce function. */
describe('06.1 Exercise for Composition', () => {

  /** Utils */
  const inc = x => x + 1;
  const dec = x => x - 1;
  const double = x => x * 2;
  const half = x => x / 2;

  const testCases = (compose, pipe) => {
    const f1 = compose(inc, dec);
    const f2 = pipe(dec, inc);
    const f3 = compose(dec, double, inc, half);
    const f4 = pipe(half, inc, double, dec);
    const f5 = compose(inc);
    const f6 = pipe(inc);
    assert.equal(f1(3), 3);
    assert.equal(f1(3), f2(3));
    assert.equal(f3(3), 4);
    assert.equal(f3(3), f4(3));
    assert.equal(f5(3), 4);
    assert.equal(f5(3), f6(3));
  }

  it('should pass all the test cases (my implementation)', () => void testCases(myCompose, myPipe));
  it('should pass all the test cases (kyle implementation)', () => void testCases(kCompose, kPipe));
})