const R = require('ramda');
const { inspect } = require('../../../functional-utils');

describe('06.2 Composition and Currying', () => {
  it('Hello', () => {
    const array = R.range(0, 50);
    inspect(array);
  });
});
