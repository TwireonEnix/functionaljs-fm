const { assert } = require('chai');
const R = require('ramda');
const { map } = require('../../../functional-utils');


describe('4.1. Lazy vs Eager evaluations', () => {

  // Output calculation occurs in the 
  it('Example with lazy evaluation', () => {
    const repeater = count => () => ''.padStart(count, 'A');
    const fiveA = repeater(5);
    const secondFiveA = fiveA(); // in the second call the string is being generated
    // count is remembered due closure.
    assert.equal(fiveA(), secondFiveA);
    assert.equal(fiveA(), 'AAAAA');
  });

  it('Example with eager evaluation', () => {
    const repeater = count => {
      const repeated = ''.padStart(count, 'A'); // caching this value via closure
      return R.concat(repeated);
    };
    const fiveA = repeater(5);

    // In this calls the string returned is the one saved in the function via closure.
    const secondFiveA = fiveA('B');
    const thirdFiveA = fiveA('B');
    assert.equal(secondFiveA, thirdFiveA);
  });

  it(`Poor man's memoization with impurity closing a variable that is being reassigned`, () => {
    const repeater = count => {
      let str;
      return () => {
        if (str === undefined) str = ''.padStart(count, 'A');
        return str;
      };
    }
    const A = repeater(5); // padding does not occur, we only cache the count value
    const a1 = A(); // calculation occurs as it the first time the function is called. 
    // A() satisfies the rules of a pure function call, but the implementation is NOT pure.
    const a2 = A(); // calculation is not made again as it was memoized from the first call.
    assert.equal(a1, a2);
    assert.equal(a2, 'AAAAA');
  });

  it('Using a memoize utilities from a fp library', () => {
    const repeater = count => R.memoizeWith(R.identity, () => {
      // console.log('called Once'); prints once no matter how many times the returning fn is called
      return ''.padStart(count, 'A')
    });
    const A = repeater(5);
    // same as above, but with functional purity provided by Ramda
    const a1 = A();
    const a2 = A();
    const a3 = A();
    assert.equal(a1, a2);
    assert.equal(a2, 'AAAAA');
    assert.equal(a1, a3);

  });

});