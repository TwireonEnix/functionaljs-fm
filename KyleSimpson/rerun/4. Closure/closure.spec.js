const { assert } = require('chai');

describe('04. Closure', () => {
  it('Practice closure', () => {
    // When we call string builder with a string (empty is valid) the new string should be concatenated
    // with the previous string remembering it functionally via closure. When the function is called
    // Without parameter it should return the built string.

    // Incorrect solution with mutation (doesn't work in all cases)
    // fails like this: -Hello, KyleSusan?!
    function strBuilder1(str) {
      return function next(v) {
        if (typeof v === 'string') {
          str += v; // functional impurity
          return next;
        }
        return str;
      };
    }

    // My solution (used undefined instead of a typeof)
    const strBuilder = str => nextStr =>
      typeof nextStr !== 'string' ? str : strBuilder(str + nextStr);

    const hello = strBuilder('Hello, ');
    const kyle = hello('Kyle');
    const susan = hello('Susan');
    const question = kyle('?')();
    const greeting = susan('!')();
    assert.equal(strBuilder('Hello, ')('')('Kyle')('.')('')(), 'Hello, Kyle.');
    assert.equal(hello(), 'Hello, ');
    assert.equal(kyle(), 'Hello, Kyle');
    assert.equal(susan(), 'Hello, Susan');
    assert.equal(question, 'Hello, Kyle?');
    assert.equal(greeting, 'Hello, Susan!');
  });
});