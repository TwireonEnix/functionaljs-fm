'use strict';

const isVowel = char => ['a', 'e', 'i', 'o', 'u'].includes(char);

const pipe = (...args) => fn => args.reduce((x, f) => f(x), fn);
// native
// const countVowels = str =>
// 	str
// 		.split('')
// 		.map(isVowel)
// 		.map(e => (e ? 1 : 0))
// 		.reduce((acc, next) => acc + next, 0);

// recursive
// Can I break this problem into some pieces that are easier to understand?
// const countVowels = str => {
// 	if (!str.length) return 0;
// 	return (isVowel(str[0]) ? 1 : 0) + countVowels(str.slice(1));
// };

// refactoring due to tweaking that first call only instead of calling again the countVowels if we can save
// a function call. Sometimes this tweaks unlocks hidden ways of optimizing the recursion.
// However is not a proper tail call position because it still has to compute a value after the function returns.
// const countVowels = str => {
// 	const [char, ...rest] = str;
// 	const first = isVowel(char) ? 1 : 0;
// 	if (!rest.length) return first;
// 	return first + countVowels(rest.join('')); // pesky first is causing trouble!
// };

// const countVowels = (str, previous = 0) => {
// 	const [char, ...rest] = str;
// 	if (!rest.length) return previous;
// 	return countVowels(rest.join(''), previous + (isVowel(char) ? 1 : 0));
// };

// Continuous passing style using an identity function
// CPS is almost never used.
// It is like a cheat, the function call is wrapped up in another function, deferring the work to it the function
// is an identity function.
// Interesting trick but it's more prone to heap space errors.
// const countVowels = (str, cont = v => v) => {
// 	const [char, ...rest] = str;
// 	const first = isVowel(char) ? 1 : 0;
// 	if (!rest.length) return cont(first);
// 	return countVowels(rest.join(''), v => cont(first + v));
// };

// Trampolines:
// Trick where instead of making a recursive call, we use an adapter function to return another function
// That will perform the recursion, so that the frame stack is never surpassed because the frame does not
// get above one.

// The implementation is kind of weird, the functional libraries may solve this in a more elegant way.
const trampoline = fn => (...args) => {
	let result = fn(...args);
	while (typeof result === 'function') result = result();
	return result;
};

//trampoline applied
const countVowels = trampoline((str, previous = 0) => {
	const [char, ...rest] = str;
	if (!rest.length) return previous;
	// the call is wrapped in a function which returns it.
	return () => countVowels(rest.join(''), previous + (isVowel(char) ? 1 : 0));
});

console.log(countVowels('The quick brown fox jumps over the lazy dog'));
