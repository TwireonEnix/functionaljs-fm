const split = 'Hello'.split('');
const concat = split.reduce((a, b) => [a, ...b], []);
// console.log(concat);

/** Archaic version of reduce, very general  */
const reduce = (reducer, initialValue, list) => {
	let ret = initialValue;
	for (let elem of list) ret = reducer(ret, elem);
	return ret;
};

const addToRecord = (record, [key, value]) => ({ ...record, [key]: value });
const listToReduce = [['name', 'kyle'], ['age', 40], ['isTeacher', true]];

const list = reduce(addToRecord, {}, listToReduce);
console.log(list);
console.log(listToReduce.reduce(addToRecord, {}));

// Composition With Reduce
const add1 = v => v + 1;
const mul2 = v => v * 2;
const div3 = v => v / 3;

const composeTwo = (fn2, fn1) => v => fn2(fn1(v));

// compose all of these functions with reduction.
const f = [div3, mul2, add1].reduce(composeTwo);
const p = [add1, mul2, div3].reduceRight(composeTwo);

console.log({ f: f(8), p: p(8) });
// Because of the previous lines we can rewrite a compose function with reduce
const compose = (...fns) => fn =>
	fns.reduceRight(
		// in this line we are performing reduction through invocation, then the next element
		// is the input of the last computation
		(x, f) => f(x),
		fn,
	);

const pipe = (...fns) => fn => fns.reduce((x, f) => f(x), fn);

const fc = compose(
	div3,
	mul2,
	add1,
);

const fp = pipe(
	add1,
	mul2,
	div3,
);

console.log({ fc: fc(8), fp: fp(8) });
