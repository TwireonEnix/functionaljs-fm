// HOF negating adapter (called complement in certain libraries, aka negated)
const not = fn => (...args) => !fn(...args);

const isOdd1 = v => v % 2 === 1;

// original const isEven = v => !isOdd(v);
// here is even more clear the relation between isOdd and isEven, point-free
const isEven1 = not(isOdd1);

console.log([1, 2].map(isOdd1));

// advanced point free with is even or odd.
const mod = y => x => x % y;
const eq = y => x => x === y;

const mod2 = mod(2);
const eq1 = eq(1);

// odd can be rewritten like this
const isOdd2 = x => eq1(mod2(x));
console.log([1, 2].map(isOdd2));
// previous odd was the foundation of function composition, taking the output of a function as
// the argument for another function.

/** Writing a compose utility (archaic) */
// const compose = (fn2, fn1) => v => fn2(fn1(v));

/** Modern compose, (piping would use reduce)*/
const compose = (...args) => v => args.reduceRight((x, f) => f(x), v);

// equational reasoning as the eq1(mod2(x)) is equivalent to fn2(fn1(v)) in the compose fn
const isOdd3 = compose(
	eq1,
	mod2,
);

console.log([1, 2].map(isOdd3));

// Archaic pipe
// const pipe = (fn1, fn2) => v => fn2(fn1(v));

// using a reverse adapter to create a pipe function of a compose function
const reverse = fn => (...args) => fn(...args.reverse());
const pipe = reverse(compose);

// equational reasoning as the eq1(mod2(x)) is equivalent to fn2(fn1(v)) in the compose fn
const isOdd4 = pipe(
	mod2,
	eq1,
);
console.log([1, 2].map(isOdd4));

//final equational reasoning because of the underline equality of the function outputs
// this line is more readable than line 21
const isOdd5 = pipe(
	mod(2),
	eq(1),
);

console.log([1, 2].map(isOdd5));

// outline: if i have this function, and i need that function, what changes need to occur if the shape
// is interchangeable
