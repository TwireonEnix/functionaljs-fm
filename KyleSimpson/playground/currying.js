const reverse = fn => (...args) => fn(...args.reverse());
const pipe = (...fns) => v => fns.reduce((x, f) => f(x), v);
const compose = reverse(pipe);

// const add = (x, y) => x + y;
// const arr = [0, 2, 3, 4, 6, 8].map(v => add(1, v));

/** In the previous example we cannot pass add directly because they have different shapes.
 * By currying it we can shape the function to be able to use it in a high order function.
 */
const add = x => y => x + y;
const arr = [0, 2, 3, 4, 6, 8].map(add(1));
// console.log(arr);

// Currying with function composition.
const sum = x => y => x + y;
const triple = x => x * 3;
const divBy = y => x => x / y; // Think of parameter order, IT MATTERS!

/** We have here some unary and binary functions. However
 * we usually require to use unary functions only for function
 * composition, because we would need an adapter every time we do not
 * have a certain shape of functions. Think of this as Legos. We cannot
 * Stick a 'one' entry piece into a two entry without it being weird.
 * It's all about the shape of the functions.
 *
 * To do this we need to curry the functions in the first place, and then
 * we should use partial application.
 * Currying allowed us to reshape those binary into unary functions which
 * ultimately allows those unary functions to be composed together.
 */

const operation = compose(
	divBy(2),
	triple,
	sum(3),
)(5);

console.log(operation);
