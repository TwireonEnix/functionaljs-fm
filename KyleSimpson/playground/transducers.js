const add1 = v => v + 1;
const isOdd = v => v % 2 === 1;
const sum = (total, v) => total + v;

const compose = (...fns) => v => fns.reduceRight((x, f) => f(x), v);

// deriving transducers
/** This way of making a mapping is impure, but transducers are focused on
 * improving performance. BUT ALL OF THESE CODE UNTIL THIS POINT IS IMPURE
 * we are mutating arrays everywhere.
 */
// const mapWithReduce = (arr, mappingFn) =>
// 	arr.reduce((list, v) => {
// 		list.push(mappingFn(v));
// 		return list;
// 	}, []);

// const filterWithReduce = (arr, predicateFn) =>
// 	arr.reduce((list, v) => {
// 		if (predicateFn(v)) list.push(v);
// 		return list;
// 	}, []);

/** Combiner: which later does not matter */
const listCombination = (list, v) => {
	list.push(v);
	return list;
};

// we should change the shape of these utilities so that we only return a reducer
// that can be composed
const mapReducer = mappingFn => combineFn => (list, v) => combineFn(list, mappingFn(v));

const filterReducer = predicateFn => combineFn => (list, v) => {
	if (predicateFn(v)) return combineFn(list, v);
	return list;
};

/** This line is setting our two transducer into a composed transducer */
const transducer = compose(
	mapReducer(add1),
	filterReducer(isOdd),
);

let list = [1, 3, 4, 6, 9, 12, 13, 16, 21];

/** first implementation */
// list = mapWithReduce(list, add1);
// list = filterWithReduce(list, isOdd);
// console.log(list.reduce(sum));

/** Second implementation */
// const value = list
// 	.reduce(mapReducer(add1)(listCombination), [])
// 	.reduce(filterReducer(isOdd)(listCombination), [])
// 	.reduce(sum);
// console.log(value);

/** Third implementation from composing the high order reducers. In this case the values, or the
 * array of numbers, is not what is flowing through the composition. The reducers are.
 */
// const value = list.reduce(transducer(listCombination), []).reduce(sum, 0);
// console.log(value);

/** There's still two reducers in play, but, list combination and sum have the same shape,
 * sum is actually a reducing function, it takes a value, and an accumulator and merges them
 * together into one. Then we do not need list combination, we can pass the sum through the
 * transducer only.
 */
const value = list.reduce(transducer(sum), 0);
console.log(value);

/** both reducers do a similar thing, pushing a value into an array and returning it. So
 *  well make a combiner. The combiner takes two values and return one, is the shape of a
 * reducer. But wait: we are hard coding the listCombination function, which we can pass it
 * as a parameter in a curried version of the function. This achieves specialization from
 * generalization.
 * What if we can compose the reducers?
 * */
