// Archaic polyfill for a mapping function
const map = (mapper, list) => {
	let newList = [];
	for (let elem of list) newList.push(mapper(elem));
	return newList;
};

const makeRecord = name => ({
	id: 'someId',
	name,
});
const records = map(makeRecord, ['Darío', 'Navarrete', 'Caballero']);
console.log(records);
