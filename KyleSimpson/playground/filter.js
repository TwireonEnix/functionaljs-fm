const isLoggedIn = user => user.session !== null;

// Archaic polyfill of a filterIN function
const filterIn = (predicate, list) => {
	let newList = [];
	for (let elem of list) {
		if (predicate(elem)) newList.push(elem);
	}
	return newList;
};

const filtered = filterIn(isLoggedIn, [
	{ userID: 42, session: 'abc' },
	{ userID: 17 },
	{ userID: 12, session: 'abcd' },
]);

console.log(filtered);
