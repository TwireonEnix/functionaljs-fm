# Pure Functions

- The functions depends ond hte input provided and not on external data that changes
- The function does not cause side effects and do not change anything outside of its scope
- The function will always return the same output given the same input.

## Side Effects Examples

- Changing a value globally
- Changing the original value of a functions argument
- Throwing an exception
- Printing to The screen or Logging
- Trigger an external process
- Invoking other functions that have side-effects

The truth is that no real program can be achieved without side effects.

> Functional programming is the process of building software by composing pure functions, avoiding shared
> state, mutable data, and side-effects. Functional programming is declarative rather than imperative, and
> application state flows through pure functions.
