## Shared State

A program is considered stateful if it is designed to remember data from events or user interactions. The
remembered information is called the state of the program. A JS program stores data in variables and objects.
The contents of these storage locations at any given moment while the program is running is considered state.

> Shared state is any variable, object, or memory space that exists in a shared scope, or as the property of
> an object being passed between scopes. A shared scope can include global scope or closure scopes.

### Avoiding mutable data
