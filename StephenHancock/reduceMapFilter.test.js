const { assert } = require('chai');
/**
 *  Reduce and reduceRight combines the elements of an array using a provided function, a reducer is a function that takes 2
 * params and combines them into one: an example: a simple add function
 *
 * Map: passes each element of the array to the function provided and returns a new array that consists of the values returned by that function
 *
 * Filter: returns a new array that is a subset of the existing array by evaluating the provided predicate (function that returns a boolean)
 *
 * All three operations are non mutable.
 */

// Curried versions extracted from functional utils, reduce is implemented natively and map and filter are specialized versions of a reduce function
const { converge, divide } = require('ramda');

const { filter, map, reduce, compose } = require('../functional-utils');

const arr = [1, 2, 3, 4, 5];

const add = (a, b) => a + b;

describe('Testing Map, Reduce and Filter', () => {
  it('Reduce', () => {
    const sum = reduce(add, 0);
    assert.strictEqual(sum(arr), 15);
  });

  it('Map', () => {
    const squared = map(x => x ** 2);
    assert.deepStrictEqual(squared(arr), [1, 4, 9, 16, 25]);
  });

  it('Filter', () => {
    const odds = filter(x => x % 2 !== 0);
    assert.deepStrictEqual(odds(arr), [1, 3, 5]);
  });

  it('Exercises', () => {
    const scores = [50, 6, 100, 0, 10, 75, 8, 60, 90, 80, 0, 30, 110];

    // it('Any scores that are below 10 needs to be multiplied by 10 and the new value included');
    // it('Remove any scores that are over 100');
    // it('Remove any scores that are 0 or below');
    // it('Sum the scores');
    // it('Provide a count for the number of scores still remaining');
    const check = v => {
      console.log(v);
      return v;
    };

    const sum = reduce(add, 0);
    // instructor wants me to count with reduce
    const count = reduce((acc, _) => acc + 1, 0);

    // could use a converge here with ramda
    const avg = converge(divide, [sum, count]);

    const result = compose(
      Math.trunc,
      avg,
      filter(s => s > 0 && s <= 100),
      map(s => (s < 10 ? s * 10 : s))
    )(scores);

    assert.strictEqual(result, 63);
  });
});
