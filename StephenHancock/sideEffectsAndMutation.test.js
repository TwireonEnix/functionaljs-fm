const { assert } = require('chai');
const { or, liftN, lensIndex, update } = require('ramda');

const compose = (...fns) => x => fns.reduceRight((y, fn) => fn(y), x);
const tap = fn => v => {
  fn(v);
  return v;
};
const identity = x => x;
const find = (prop, value) => data => data.find(v => v[prop] === value);
const inc = x => x + 1;

describe('Avoiding mutation and shared state', () => {
  it('should explain mutation', () => {
    let count = 0;

    /** Here this 'function' (actually is not) increments and returns the value of the variable count
     * but it changes its value
     *
     * - no function should modify values outside of itself.
     */
    let increment = () => {
      count++;
      return count;
    };

    /**  */
    increment();
    assert.strictEqual(count, 1);

    /** no side effects
     * This is a pure function.
     */
    let correctIncrement = num => num + 1;

    assert.strictEqual(correctIncrement(1), 2);
  });

  it('implement exercise', () => {
    /** Original */
    // var currentUser = 0; //useless
    var users = [
      { name: 'James', score: 30, tries: 1 },
      { name: 'Mary', score: 110, tries: 4 },
      { name: 'Henry', score: 80, tries: 3 }
    ];
    // var updateScore = newAmt => {
    //   users[currentUser].score += newAmt;
    // }

    // var returnUsers = () => users;

    // var updateTries = () => {
    //   users[currentUser].tries++
    // };

    // var updateUser = newUser => {
    //   currentUser = newUser;
    // }

    /** My implementation
     * IMO currentUser should not be an external id. Lets find users from their names.
     */

    const generalUpdate = prop => (fn, arg) => userName => data =>
      compose(
        v => v && [...data.filter(n => n.name !== v.name), { ...v, [prop]: arg ? fn(arg) : fn(v[prop]) }],
        find('name', userName)
      )(data);

    const updateScore = (userName, score) => generalUpdate('score')(identity, score)(userName);
    const updateTries = userName => generalUpdate('tries')(inc)(userName);
    const returnUsers = identity;

    const firstUpdate = updateScore('James', 120)(users);
    assert.strictEqual(find('name', 'James')(firstUpdate).score, 120);

    const secondUpdate = updateTries('Mary')(users);
    assert.strictEqual(find('name', 'Mary')(secondUpdate).tries, 5);

    const original = returnUsers(users);
    assert.deepStrictEqual(original, users);
  });

  it('instructor implementation', () => {
    // Instructor also does not use currentUser, instead searches by name, but using imperative ways, which i dislike, although they may be more performant on some cases
    var users = [
      { name: 'James', score: 30, tries: 1 },
      { name: 'Mary', score: 110, tries: 4 },
      { name: 'Henry', score: 80, tries: 3 }
    ];

    // wrong param order IMO, make currying difficult
    // not using currying, but it's understandable as these are the first lessons
    var getUser = function (arr, name) {
      // i only like a for loop when creating a vanilla reduce function
      for (let i = 0, { length } = arr; i < length; i++) {
        if (arr[i].name === name) {
          return arr[i];
        }
      }
      return null;
    };
    // instructor uses mutation, which i don't like

    var updateScore = function (user, newAmt) {
      if (user) {
        user.score += newAmt; // I've not one single time in my fp usage have i ever use this increment operator
        return user;
      }
    };

    // instructor deletes this function, ignoring identity function utility
    // var returnUsers = () => users;

    var updateTries = function (user) {
      if (user) {
        user.tries++; // again with the mutation operations.
        return user;
      }
    };

    var updateUser = function (user) {
      // damn son, this is not functional in any way. this is not declarative, this code sucks.
      // also is destructive, users are mutating outside of the function's scope
      for (let i = 0, { length } = users; i < length; i++)
        if (users[i].name.toLowerCase() === user.name.toLowerCase()) users[i] = user;
    };

    let usr = getUser(users, 'Henry');
    let usr1 = updateScore(usr, 30);
    let usr2 = updateTries(usr1);

    // here original users is mutating. NOT GOOD! but instructor really did this
    updateUser(usr2);

    /** However, He explains this in subsequent lectures. */
  });
});
