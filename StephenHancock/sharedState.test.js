'use strict';
const { assert } = require('chai');

describe('Shared State Examples', () => {
  it('Clarify what const really does', () => {
    /** const only makes variables not re-assignable, however, it won't prevent mutation of reference-passed objects  */
    const arr = [3, 4, 5, 1];
    const cpy = [...arr];
    arr.sort(); // mutation of the original object;
    assert.notDeepEqual(arr, cpy);
    assert.isNotFrozen(arr);

    // We could freeze the object with the method of the Object interface
    Object.freeze(arr);
    assert.isFrozen(arr);

    const mutate = x => x.splice(0, 2);
    // mutate(arr) // Throws
  });

  it('Cloning object techniques', () => {
    // Object.assign()
    /** Originally this was used to merge two objects, but it is used also to clone objects */

    const obj = {
      fName: 'Me',
      lName: 'Test',
      score: 80,
      isComplete: false,
      nested: {
        q1: 2,
        q2: 'Hello'
      }
    };

    const obj2 = Object.assign({}, obj);
    // mutate one of the two
    delete obj2.isComplete;
    assert.hasAllKeys(obj, ['fName', 'lName', 'score', 'isComplete', 'nested']);
    assert.hasAllKeys(obj2, ['fName', 'lName', 'score', 'nested']);
    /** However, the important limitation of Object.assign() is that it only clones primitive values
     * any object inside the original as a property is passed as reference  */

    // JSON.stringify
    const obj3 = JSON.parse(JSON.stringify(obj));

    // Instructor does not cover the spread operator for cloning;
    // However this have the same caveat as Object.assign(), so best way of cloning is using a library method (ramda in my case)
    const obj4 = { ...obj };
    obj.nested.q1 = 123;

    const arr2 = [{ a: 1 }, { b: 2, c: [{ d: 3 }] }];
    const clonedArr = [...arr2];
    clonedArr[1].c[0].d = 4;
    // console.log({ obj, obj4 });
    // console.log({ clonedArr });
  });
});
