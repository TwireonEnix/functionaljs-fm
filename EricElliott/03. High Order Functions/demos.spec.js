const { expect } = require('chai');
const { reduce } = require('../../functional-utils');

describe('High Order Functions', () => {
  it('should reduce an array into a value', () => {
    const add = (a, b) => a + b;
    expect(reduce(add, 0, [1, 2, 3])).to.eq(6);
  });

  it('should implement filter using the previously written reduce', () => {
    const filter = fn => arr => reduce((a, c) => (fn(c) ? a.concat([c]) : a), [], arr);
    const highPass = cutoff => n => n >= cutoff;
    const gte3 = highPass(3);
    expect(filter(gte3)([1, 2, 3, 4])).to.deep.equal([3, 4]);
  });

  it('should implement map using the reduce function', () => {
    const map = fn => arr => reduce((a, c) => [...a, fn(c)], [], arr);
    const double = n => n * 2;
    expect(map(double)([1, 2, 3, 4])).to.deep.equal([2, 4, 6, 8]);
  });
});
