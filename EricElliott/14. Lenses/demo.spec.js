const { assert } = require('chai');

// naive lenses implementations of view and set functions.
// They're naive because they only work with objects. In production we should use
// well tested lenses.
const view = (lens, context) => lens.view(context);
const set = (lens, value, context) => lens.set(value, context);

//
const lensProp = prop => ({
  view: context => context[prop],
  set: (value, context) => ({
    ...context,
    [prop]: value
  })
});

const object = {
  a: 'foo',
  b: 'bar'
};

describe.only('Lenses', () => {
  const aLens = lensProp('a');
  const bLens = lensProp('b');

  it('should create view lenses for the test object', () => {
    const a = view(aLens, object);
    const b = view(bLens, object);
    assert.strictEqual(a, 'foo');
    assert.strictEqual(b, 'bar');
  });

  it('should set a value into the context using the aLens', () => {
    const changeB = v => set(aLens, v, context);
    const newB = view(aLens, changeB('fizz'));
    assert.strictEqual(newB, 'fizz');
  });
});
