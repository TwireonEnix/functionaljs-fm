const R = require('ramda');
const toExp = (v, x) =>
  require('chai')
    .expect(v)
    .to.eq(x);

describe('Var Let Const', () => {
  it('Var', () => {
    // var can be re-declared several times.
    var x = 1;
    var x = 2;
    let y = 2;
    y = '';
    toExp(x, 2);
    toExp(y, '');
  });

  it('Functions', () => {
    const double = x => x * 2;
    function doubleB(x) {
      return x * 2;
    }
    // Function.prototype.toString() returns its declaration.
    toExp(double.toString(), 'x => x * 2');
  });
  it('Default parameters', () => {
    const orZero = (x = 0) => x;
    // only undefined sets the value to the default
    toExp(orZero(), 0);
    toExp(orZero(null), null);
    toExp(orZero(1), 1);
    // Default parameters don't count towards the fn length prop. Which will mess with
    // some libraries curry, in those cases we can use curryN to override this behavior by
    // explicitly telling curry how many args it will expect.
    toExp(orZero.length, 0);
  });

  it('Currying', () => {
    // A magic spell
    const curry = (f, arr = []) => (...args) =>
      (a => (a.length === f.length ? f(...a) : curry(f, a)))([...arr, ...args]);
    const add = (x, y) => x + y;
    const curriedAdd = curry(add);
    const curriedAdd2 = R.curryN(2, add);
    const inc = curriedAdd(1);
    const add3 = curriedAdd2(3);
    toExp(inc(4), 5);
    toExp(add3(5), 8);
  });
});
