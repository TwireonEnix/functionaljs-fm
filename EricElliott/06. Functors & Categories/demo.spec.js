const { assert } = require('chai');
const { compose } = require('../../functional-utils');

describe('Functors Laws', () => {
  const mappable = [21, 42, 63];

  it('should prove the functor law identity', () => {
    const identity = x => x;
    assert.deepStrictEqual(
      mappable.map(identity), //
      mappable,
      'Functors are equal with a identity operation'
    );
  });

  it('should prove the functor law composition', () => {
    const inc = n => n + 1;
    const double = n => n * 2;

    const a = mappable.map(inc).map(double);
    const composedIncDouble = compose(double, inc); // compose operates RTL
    const b = mappable.map(composedIncDouble);

    assert.deepStrictEqual(a, b);
  });

  it('should build a simple functor', () => {
    const Identity = v => ({
      map: fn => Identity(fn(v))
    });

    const id = x => x;

    const u = Identity(3);
    // u.map(console.log);
  });
});
