const { expect } = require('chai');

const tap = fn => v => {
  fn(v);
  return v;
};

const log = tap(console.log);

describe('Currying and Point-Free Style', () => {
  it('compose in vanilla with point-free style', () => {
    const compose = (...fn) => v => fn.reduceRight((a, f) => f(a), v);
    const inc = n => n + 1;
    const double = n => n * 2;

    // point free ;
    const incAndDouble = compose(double, inc);
    // const incAndDouble = compose(double, log, inc, log); // using logs for debug
    expect(incAndDouble(20)).to.eq(42);
  });
});
