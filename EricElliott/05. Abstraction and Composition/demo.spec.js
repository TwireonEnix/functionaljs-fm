const { assert } = require('chai');
const { curry } = require('../../functional-utils');

describe('Abstraction', () => {
  /** Abstraction is key to doing more with less code. */
  const double = n => n * 2;
  const add = (a, b) => a + b; // not curried because of reducer

  it('specialized map from Array.prototype.map() as a data last fn', () => {
    const map = f => arr => arr.map(f);
    const doubleAll = map(double);
    assert.deepEqual(doubleAll([1, 2, 3]), [2, 4, 6], 'values are equal');
  });

  it('should rewrite reduce for a data last function', () => {
    const reduce = curry((reducer, initial, list) => list.reduce(reducer, initial, list));
    const sum = reduce(add, 0);
    assert.strictEqual(sum([1, 2, 3]), 6, 'sum is 6');
  });
  it('should rewrite compose as if JavaScript did not provide reduceRight', () => {
    const curryAdd = curry(add);
    const compose = (...fns) => x => fns.reverse().reduce((v, f) => f(v), x);
    const incAndDouble = compose(double, curryAdd(1));
    assert.strictEqual(incAndDouble(2), 6);
  });
});
