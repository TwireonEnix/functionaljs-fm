const compose = (...fn) => v => fn.reduceRight((a, f) => f(a), v);

const curry = (f, arr = []) => (...args) =>
  (a => (a.length === f.length ? f(...a) : curry(f, a)))([...arr, ...args]);

/** Imperative implementation of a general reduce function */
function reduce(reducer, initial, arr) {
  let acc = initial;
  for (let i = 0, { length } = arr; i < length; i++) acc = reducer(acc, arr[i]);
  return acc;
}

const filter = curry((fn, arr) => reduce((a, c) => (fn(c) ? a.concat([c]) : a), [], arr));

const map = curry((fn, arr) => reduce((a, c) => [...a, fn(c)], [], arr));

// reverse adapter, inverts the order of the arguments. (reverse is destructive
// according to mdn, but spread makes a copy of the original array);
const reverse = fn => (...args) => fn(...args.reverse());

const inspect = v => {
  console.dir(compose(JSON.parse, JSON.stringify)(v), { depth: null });
  return v;
};

module.exports = {
  compose,
  curry,
  reduce: curry(reduce),
  filter,
  map,
  reverse,
  inspect
};
