const { assert } = require('chai');
const R = require('ramda');

describe('FP with Ramda', () => {
  /**
   * Restrictions with using ramda;
   * - All functions are pure, we should compose with pure functions too
   * - All ramda functions are curried by default
   *
   */

  it('add new value to array', () => {
    const ar = [1, 2, 3];
    const newAr = R.append(4, ar);
  });
});

describe('Composition', () => {
  it('should rewrite an imperative function as a composition', () => {
    const phrase = 'This is composition @';
    const toSlug = input => {
      const words = input.split(' ');
      const lowerCasedWords = words.map(w => w.toLowerCase());
      const slug = lowerCasedWords.join('-');
      const encoded = encodeURIComponent(slug);
      return encoded;
    };
    const slug = toSlug(phrase);

    /** Using ramda to compose this function with its helpers */
    const ramdaToSlug = R.compose(
      // composition goes from bottom to top, or right to left
      encodeURIComponent,
      R.join('-'),
      R.map(R.toLower),
      R.split(' ')
    );

    const slug2 = ramdaToSlug(phrase);

    assert.strictEqual(slug, slug2);
  });

  it('converge', () => {
    const validAr = [6, 3, 4, 5, 2];
    const invalidAr = [3, 4, 6, 1];

    const isFirstElemBiggest = arr => arr[0] === R.apply(Math.max)(arr);

    assert.isTrue(isFirstElemBiggest(validAr));
    assert.isFalse(isFirstElemBiggest(invalidAr));

    const convergeFirstBiggest = R.converge(R.equals, [R.head, R.apply(Math.max)]);
    assert.isTrue(convergeFirstBiggest(validAr));
    assert.isFalse(convergeFirstBiggest(invalidAr));
  });

  it('filter with where', () => {
    const products = [
      { name: 'Jacket', price: 50, category: 'clothes', count: 20 },
      { name: 'Boots', price: 120, category: 'clothes', count: 30 },
      { name: 'Iphone', price: 600, category: 'electronics', count: 5 },
      { name: 'Ipad', price: 300, category: 'electronics', count: 10 }
    ];

    /**  */
    const productNamesWithConditions = R.compose(
      R.pluck('name'),
      R.filter(
        R.where({
          category: R.equals('clothes'),
          count: R.gt(50),
          price: R.gt(100)
        })
      )
    );
    const names = productNamesWithConditions(products);
    assert.deepStrictEqual(['Jacket'], names);
  });

  it('conditions', () => {
    // usage of ifElse ramda function
    // usage of when and unless, when -> predicate true: run fn, unless -> predicate false: run fn
    const lengthIsMoreThan5 = R.o(R.lt(5), R.length);
    const truncateIfLong = R.when(lengthIsMoreThan5, R.o(R.flip(R.concat)('...'), R.take(5)));
    const truncated = truncateIfLong('123456');
    assert.strictEqual(truncated, '12345...');
  });

  it('change props with lenses', () => {
    const user = {
      name: 'John',
      surname: 'Doe'
    };

    const nameLens = R.lens(R.prop('name'), R.assoc('name')); // analog to lensProp, or lensPath
    const name = R.view(nameLens, user);
    assert.strictEqual(name, 'John');

    const updateName = R.over(nameLens, R.toUpper, user);
    assert.strictEqual(updateName.name, 'JOHN');
  });

  it('use with', () => {
    /** useWith works similar to converge, but allows more than one parameter, the arity of the function must coincide with the second array argument of
     * the useWith Fn, then each parameter that the function is called with is passed in order to the functions in the array, computed and reduced with the
     * first function param of the general useWith fn
     */
    const weirdConcat = R.useWith(R.concat, [R.toLower, R.toUpper]);
    assert.strictEqual(weirdConcat('RAMDA', 'isCool'), 'ramdaISCOOL');

    const weirdMultiply = R.useWith(R.multiply, [R.inc, R.add(10)]);
    /** here, 3 is incremented and 5 is added to 10, then both are multiplied, so array length should match the arity of the 'reduce' function */
    assert.strictEqual(weirdMultiply(3, 5), 60);

    /** arguments can also be passed as is if one argument does not need transformations, but ramda recommends to placing an identity to deal with such cases */
    const triAdd = R.curry((a, b, c) => a + b + c);
    const transformAndAdd = R.useWith(triAdd, [R.inc, R.dec]);
    const correctTransformAndAdd = R.useWith(triAdd, [R.inc, R.dec, R.identity]);

    /** as a final note, the resulting function is also curried provided that the transformation function is also curried */
    assert.strictEqual(transformAndAdd(3, 2)(4), correctTransformAndAdd(3)(2)(4));
  });
});
