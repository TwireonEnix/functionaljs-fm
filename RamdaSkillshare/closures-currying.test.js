const { assert } = require('chai');
const { curry, map } = require('ramda');

describe('Closures And Currying', () => {
  /** example function */
  const addTo = passed => {
    const add = inner => {
      return passed + inner;
    };
    return add;
  };

  const add = (a, b) => a + b;

  it('curried add', () => {
    const curriedAdd = curry(add);

    const two = curriedAdd(1, 1);
    assert.strictEqual(two, 2);

    const inc = curriedAdd(1);
    assert.strictEqual(inc(3), 4);
  });

  it('more complex curry use', () => {
    const obj = [{ id: 1 }, { id: 2 }, { id: 3 }];
    // write a simple 'prop' function
    const prop = curry((p, o) => o[p]);

    const getIds = map(prop('id'));

    assert.deepStrictEqual(getIds(obj), [1, 2, 3]);
  });

  it('write a curry function from scratch', () => {
    const poorCurry = fn => {
      const arity = fn.length; // storing in closure total amount of args
      return function recur(...args) {
        return args.length >= arity ? fn(...args) : (...moreArgs) => recur(...args.concat(moreArgs)); // this will be called until we have all the original number of arguments, otherwise it will return functions.
      };
    };

    const add = (a, b, c) => a + b + c;
    const poorCurriedAdd = poorCurry(add);

    const sixWithCurry = poorCurriedAdd(1, 2)(3);
    assert.strictEqual(sixWithCurry, 6);
  });
});
